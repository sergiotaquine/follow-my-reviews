// ==UserScript==
// @name         Gitlab Reviewer
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Show on which MR you interracted
// @author       SergioTaquine
// @match        /^https:\/\/\w(?=\S*['-])([a-zA-Z0-9-_]+)\/merge_requests/g
// @grant        GM.setValue
// @grant        GM.getValue
// @grant        GM.deleteValue
// @grant        GM.listValues
// ==/UserScript==

const DAYS_BEFORE_FLUSHING = 30; // How much time wee keep local saves
const MS_TO_DAY = 86400000; // How many miliseconds is 1 day
const { href: currentHref, pathname: currentPathName } = window.location || {};
const homeMergeRequestsPathName = currentPathName.substring(0, currentPathName.indexOf('merge_requests') + "merge_requests".length);

const getInteractions = async(updatedInteractionsList) => {
    const allMergeRequests = document.querySelectorAll('.merge-request');

    for (let mr of allMergeRequests) {
        const link = mr.getElementsByTagName('a')[0].href;
        const currentMergeRequest = updatedInteractionsList.find(({name}) => name.startsWith(link)) || {};
        const { value: { interactionsNumber, approved } = {} } = currentMergeRequest;
        if (interactionsNumber || approved){
            const title = mr.querySelectorAll('.merge-request-title-text')[0];
            title.prepend(`${approved ? '✅': '❌' } 👨‍💻(${interactionsNumber})`);
        }
    }
}

const flushOldValues = async() => {
    // List all values and check if we have to remove some old stuffs (Decrease localStorage usage)
    const allValues = await GM.listValues();
    let currentValues = [];

    if (allValues && allValues.length > 0) {
        for (let value of allValues) {
            const valueParsed = JSON.parse(await GM.getValue(value));
            const { date } = valueParsed;
            if (Date.now() > date + (DAYS_BEFORE_FLUSHING * MS_TO_DAY)) {
                await GM.deleteValue(value);
            } else {
                currentValues.push({
                    name: value,
                    value: valueParsed
                });
            }
        }
    }
    return currentValues;
};

const listeningInteractions = async() => {
    const reviewButtonSelector = 'js-publish-draft-button';
    const commentButtonSelector = 'js-comment-button';

    document.addEventListener('click', async(e) => {
        const button = e.target.closest("button");
        if (button) {
            const isComment = button.classList.contains(reviewButtonSelector) || button.classList.contains(commentButtonSelector);
            const isApproval = button.dataset['qaSelector'] === 'approve_button';
            const existingObject = await GM.getValue(currentHref);

            const { interactionsNumber = 0, approved } = existingObject ? JSON.parse(existingObject): {};

            const newObject = JSON.stringify({
                name: currentHref,
                date: Date.now(),
                interactionsNumber: isComment ? interactionsNumber + 1: interactionsNumber,
                approved: isApproval || approved
            });
            GM.setValue(currentHref, newObject);
        }
    });
};

(async() => {
    'use strict';

    const isHome = currentPathName === homeMergeRequestsPathName || currentPathName === homeMergeRequestsPathName+'/' ;

    if (isHome) {
        const updatedInteractionsList = await flushOldValues();
        getInteractions(updatedInteractionsList);
    } else {
        listeningInteractions();
    }
})();
