# Follow my reviews

## Description
A tampermonkey script to add a review following system, working with localstorage.
First used on a private Gitlab instance, not tested elsewhere yet.


## How to use it
To use this script I use Tampermonkey.

*   Download Tampermonkey Chrome extension on the official Chrome Web store.
*   Simply add the raw script to a new userscript in Tampermonkey.

**Please if you can read code, read it first and never add a script you find on the web if you don't trust the content or the developper who did it.**


> If you're lost here and don't know how the web works : Scripts could be very dangerous.
